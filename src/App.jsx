import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import Posts from './Posts';
import Post from './Posts/Post';
import NewPost from './Posts/NewPost';

const client = new ApolloClient({
  uri: 'https://api-euwest.graphcms.com/v1/cjlxsis6f028a01dm6fvy0xzi/master'
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <header>
          Header
        </header>
        <Router>
          <Switch>
            <Route exact path="/" component={ Posts }/>
            <Route path="/post/:id" component={ Post }/>  
            <Route path="/newpost" component={ NewPost }/>  
          </Switch>
        </Router>        
      </ApolloProvider>
    );
  }
};

export default App;
