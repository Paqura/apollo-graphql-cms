import React, { Component } from "react";
import { Query } from "react-apollo";
import { Link } from "react-router-dom";
import gql from "graphql-tag";

class Posts extends Component {
  render() {
    return (
      <ul>
        <li>
          <Link to={'/newpost'}>Add new post</Link>
        </li>
        <Query 
          query={POSTS_QUERY}
        >        
          {({ loading, data }) => {
            if (loading) return "Loading...";
            const { posts } = data;
            return posts.map(post => (
              <li key={post.id}>
                <Link to={`post/${post.id}`}>
                  <h1>{post.title}</h1>
                </Link>
              </li>
            ));
          }}
        </Query>
      </ul>
    );
  }
}

export default Posts;

const POSTS_QUERY = gql`
  query allPosts {
    posts {
      id
      title
    }
  }
`;
