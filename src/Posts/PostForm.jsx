import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class PostForm extends Component {
  static propTypes = {
    onHandleSubmit: PropTypes.func.isRequired,
    post: PropTypes.object
  };

  static defaultProps = {
    post: {}
  }

  state = {
    id: this.props.post.id || '',
    title: this.props.post.title || '',
    body: this.props.post.body || ''
  };

  handleChange = (evt) => {
    const formData = {}; 
    formData[evt.target.name] = evt.target.value;
    this.setState({...formData });
  };

  render() {
    const { title, body } = this.state;
    const { onHandleSubmit, post  } = this.props;
    return (
      <form onSubmit={(evt) => {
        evt.preventDefault();
        onHandleSubmit({
          variables: {
            title,
            body,
            id: post.id
          }
        })
          .then(() => {
            this.setState({
              title: '',
              body: ''
            })
          })
          .catch(err => console.log(err));
      }}>
      <p>
        <input 
          type="text" 
          name="title" 
          placeholder="Post title"
          value={title}
          onChange={this.handleChange}
        />
      </p>
      <p>
        <textarea 
          name="body" 
          placeholder="Post body"
          value={body}
          onChange={this.handleChange}
        />
      </p>
      <p>
        <button>Submit</button>
      </p>
    </form>
    )
  }
}
